import datetime

from behave import *
from django.utils import timezone
from django.test import Client
from polls.models import Question

import parse

@parse.with_pattern(r"\-?\d+")
def parse_number(text):
    return int(text)

register_type(Number=parse_number)


@given(u'we have a question which is published {n:Number} hours from now')
def step_impl(context, n):
    time = timezone.now() + datetime.timedelta(hours=n)
    question = Question(pub_date=time)
    question.save()
    context.question = question

@then(u'the question was recently published')
def step_impl(context):
    assert context.question.was_published_recently() is True

@then(u'the question was not recently published')
def step_impl(context):
    assert context.question.was_published_recently() is False

@when(u'I go to "{url}"')
def step_impl(context, url):
    """
    :type context: behave.runner.Context
    """
    context.response = context.test.client.get(url)

@then(u'I should see "{text}"')
def visit(context, text):
    response = context.response
    context.test.assertContains(response, text)

@then(u'I should not see "{text}"')
def visit(context, text):
    response = context.response
    context.test.assertNotContains(response, text)

@then(u'i see a "{status_code:Number}" status code')
def step_impl(context, status_code):
    response = context.response
    assert response.status_code is status_code

