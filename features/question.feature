Feature: question model

  Scenario: recently published includes questions whose pub_date is in near past
     Given we have a question which is published -1 hours from now
      Then the question was recently published

  Scenario: recently published excludes questions whose pub_date is in far past
     Given we have a question which is published -100 hours from now
      Then the question was not recently published

  Scenario: recently published excludes questions whose pub_date is in the future
     Given we have a question which is published 1 hours from now
      Then the question was not recently published

