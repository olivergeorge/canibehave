Feature: poll pages

  Scenario: Accessing empty polls list
      When i go to "/polls/"
      Then i see a "200" status code
      Then i should see "No polls are available."

  Scenario: Accessing polls list
     Given we have a question which is published -1 hours from now
      When i go to "/polls/"
      Then i see a "200" status code
      Then i should not see "No polls are available."

